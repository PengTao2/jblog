package com.newflypig.jblog.controller.interceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.newflypig.jblog.model.BlogCommon;

public class BlogCommonInterceptor implements HandlerInterceptor {

	@Resource(name = "blogCommon")
	private BlogCommon blogCommon;

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		//加上if判断防止有些情况modelAndView抛出空指针异常
		if(modelAndView != null){
			modelAndView.addObject("blogCommon", blogCommon);
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
	}
}
