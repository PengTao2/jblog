package com.newflypig.jblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.newflypig.jblog.model.Result;
import com.newflypig.jblog.service.ITagService;

@Controller
@RequestMapping("/tags")
public class TagController {
	
	@Autowired
	private ITagService tagService;
	
	@RequestMapping(value="/admin/list", method=RequestMethod.GET)
	public ModelAndView list(){
		List<String> list = this.tagService.findAllFromTable();
		
		Result result = new Result();
		
		String resultStr = list.stream().reduce(
			new String(),
			(resultTmp, item) -> resultTmp += ("\"" + item + "\","),
			(u,t) -> u
		);
		
		resultStr = "[" + resultStr.substring(0, resultStr.length() -1) + "]";
		result.setData(resultStr);
		
		return new ModelAndView("ajaxResult", "result", result);
	}
}
