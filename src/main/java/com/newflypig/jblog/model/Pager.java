package com.newflypig.jblog.model;

import java.util.List;

/**
 *  泛型分页类，对数据库中的各数据进行分页的封装
 *	@author newflypig
 *	time：2015年12月6日
 *
 */
public class Pager<T> {
	//分页对象的List集合
	private List<T> data;
	
	//共几页
	private int totalPages;
	
	//动态数组存放URL地址
	private List<String[]> urls;
	
	//当前页数
	private int currentPage;

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalSize(int totalPages) {
		this.totalPages = totalPages;
	}

	public List<String[]> getUrls() {
		return urls;
	}

	public void setUrls(List<String[]> urls) {
		this.urls = urls;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

}
