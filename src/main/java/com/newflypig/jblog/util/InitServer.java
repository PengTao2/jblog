package com.newflypig.jblog.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.newflypig.jblog.model.BlogCommon;
import com.newflypig.jblog.service.IBlogService;

public class InitServer implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	private BlogCommon blogCommon;
	
	@Autowired
	private IBlogService blogService;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if(event.getApplicationContext().getParent() == null){
			//需要执行的逻辑代码，当spring容器初始化完成后就会执行该方法。
			if(blogCommon.getBlogInit())
				return;
			else{
				this.blogService.updateStatics();
				
				blogCommon.setBlogInit(true);
			}
			
	    }
	}
}
