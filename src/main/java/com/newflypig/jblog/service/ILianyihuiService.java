package com.newflypig.jblog.service;

import java.util.List;

import com.newflypig.jblog.model.Lianyihui;

public interface ILianyihuiService extends IBaseService<Lianyihui>{

	List<Lianyihui> findForShow();

	Lianyihui findByHash(String hash);

}
